<?php 

include($_SERVER['DOCUMENT_ROOT'].'/header.php');

$userid = ( isset($_GET['uid']) ) ? preg_replace('/\D/', '', $_GET['uid']): null;
$page = (!isset($_GET['page'])) ? 1: $_GET['page'];
$data = $blog->pull_blogs( $userid, $page );

?>

    <header class="masthead">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>Blogger</h1>
                        <span class="subheading">Start Blogging and be a Blogger</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
    	<div class="row">
    		<div class="col-lg-8 col-md-10 mx-auto">

                <?php if ($data['total_results'] > 0): ?>
                    
                    <?php 
                    
                    while($res = $data['blogs']->fetch(PDO::FETCH_ASSOC)): ?>
                        
                        <?php $author = $blog->get_author($res['user_id']); ?>
                        <div class="post-preview">
                            <a href="<?php echo $app->site_url().'/blog.php?id='.$res['id']; ?>">
                                <h2 class="post-title"><?php echo $res['title']; ?></h2>
                                <h3 class="post-subtitle">
                                    <?php echo $app->shorten_text($res['content'], 60, ' ', true); ?>
                                </h3>
                            </a>
                            <p class="post-meta">Posted by
                                <a href="<?php $app->siteurl(); ?>/profile.php?uid=<?php echo $author['uid']; ?>"><?php echo ucwords($author['first_name'] .' ' . $author['last_name']); ?></a>
                            on <?php echo date('F j, Y',strtotime($res['created_at'])); ?></p>
                        </div>
                        <hr>

                    <?php endwhile; ?>

                <?php endif; ?>

    			
    			<!-- Pager -->
    			<div class="clearfix">
    				<!-- <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a> -->

                    <nav aria-label="Page navigation float-right">
                        <ul class="pagination">

                            <?php for ($page=1; $page <= $data['total_pages'] ; $page++):  ?>
                                
                                <?php if ($data['total_pages'] == $page ): ?>
                                    <li>
                                        <a href="<?php echo "?page=$page"; ?>" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                <?php elseif ($page==1) : ?>
                                    <li>
                                        <a href="<?php echo "?page=$page"; ?>" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                <?php else : ?>
                                    <li>
                                        <a href='<?php echo "?page=$page"; ?>' class="links">
                                            <?php  echo $page; ?>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                
                            <?php endfor; ?>
                            
                        </ul>
                    </nav>
    			</div>
    		</div>
    	</div>
    </div>
<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>