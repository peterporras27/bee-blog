-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 13, 2019 at 04:06 AM
-- Server version: 5.7.17
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` text,
  `content` longtext,
  `user_id` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `content`, `user_id`, `created_at`) VALUES
(12, 'Man must explore, and this is exploration at its greatest', 'Problems look mighty small from 150 miles up', 10, '2019-08-27'),
(13, 'I believe every human has a finite number of heartbeats. I don\'t intend to waste any of mine.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do\r\neiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim\r\nveniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea\r\ncommodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit\r\nesse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat\r\ncupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est\r\nlaborum.', 10, '2019-08-27'),
(21, 'Science has not yet mastered prophecy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do\neiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim\nveniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea\ncommodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit\nesse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat\ncupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est\nlaborum.', 10, '2019-08-27'),
(22, 'Failure is not an option', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do\neiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim\nveniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea\ncommodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit\nesse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat\ncupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est\nlaborum.', 10, '2019-08-27'),
(23, 'Amazon Rain Forest Fire killed many animals', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do\r\neiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim\r\nveniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea\r\ncommodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit\r\nesse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat\r\ncupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est\r\nlaborum.', 10, '2019-08-27'),
(24, 'GlobalFoundries sues TSMC, wants U.S. import ban on some products', '<p>SHANGHAI (Reuters) - Contract chipmaker GlobalFoundries sued\r\nlarger\r\nrival and Apple supplier TSMC for patent infringement, seeking to\r\nstop\r\nimports into the United States and Germany of products made with the\r\nallegedly infringed technologies.</p>\r\n\r\n<p>In lawsuits filed on Monday in the United States and Germany,\r\nGlobalFoundries also sought unspecified â€œsignificantâ€ damages\r\nfrom\r\nTaiwan Semiconductor Manufacturing Co (TSMC) (2330.TW) based on the\r\nTaiwanese firmâ€™s unlawful use of its technology in its â€œtens of\r\nbillions of dollars of salesâ€.</p>', 10, '2019-08-27'),
(25, 'Why do we need modules at all?', 'This is a brain-dump-stream-of-consciousness-thing. I\'ve been\r\nthinking about this for a while.\r\n\r\nI\'m proposing a slightly different way of programming here\r\nThe basic idea is\r\n\r\n    - do away with modules\r\n    - all functions have unique distinct names\r\n    - all functions have (lots of) meta data\r\n    - all functions go into a global (searchable) Key-value database\r\n    - we need letrec\r\n    - contribution to open source can be as simple as\r\n      contributing a single function\r\n    - there are no \"open source projects\" - only \"the open source\r\n      Key-Value database of all functions\"\r\n    - Content is peer reviewed\r\n\r\nThese are discussed in no particular order below:\r\n\r\nWhy does Erlang have modules?\r\n\r\nThere\'s a good an bad side to modules:\r\n\r\nGood: Provides a unit of compilation, a unit of code\r\ndistribution. unit of code replacement\r\n\r\nBad: It\'s very difficult to decide which module to put an individual\r\nfunction in. Break encapsulation (see later)\r\n', 16, '2019-08-27'),
(26, 'Google Calendar Event Injection with MailSniper', '<p>Google Calendar is one of the many features provided to those who\nsign up for a Google account along with other popular services such as\nGmail and Google Drive. Following email, the calendar is probably the\nsecond most used pieces of productivity software in an enterprise. It\nallows employees to schedule out the work week so that things can\nactually get accomplished. Google has an interesting feature that\nautomatically adds various events to your calendar. If your Google\naccount receives an email stating you have booked flights,\nreservations for a dinner, or even movie tickets, Google will\nautomatically add these events to your calendar.</p>\r\n\r\n<p>Black Hills Information Security took a deeper look at how these\nevents were being generated. BHIS discovered that it was not required\nto send an email to a target to create an event on their calendar.\nAdditionally, there are security controls that can be enabled on a\nGoogle account that attempt to prevent this from happening, but BHIS\ndiscovered bypasses to these controls. In this post BHIS will discuss\nthis â€œEvent Injectionâ€ vulnerability, the risks associated with\nit, and how to exploit it using MailSniper.</p>\r\n\r\n<p>Black Hills Information Security reported this issue to Google. See\nthe section titled â€œTimeline of Disclosureâ€ below for\ndetails.</p>\r\n', 16, '2019-08-27');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `user_id`, `blog_id`, `created_at`) VALUES
(2, 'Hopefully this should do it.', 10, 23, '2019-08-27'),
(3, 'This should appear on the top most portion.', 10, 23, '2019-08-27'),
(4, 'hahaha! not true! these are lies! nothing but lies!', 16, 24, '2019-08-27'),
(5, 'Why oh why so harsh?', 10, 24, '2019-08-28'),
(7, 'Nice article you got here.', 10, 26, '2019-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8_bin NOT NULL,
  `password` varchar(32) COLLATE utf8_bin NOT NULL,
  `first_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `middle_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(1000) COLLATE utf8_bin NOT NULL,
  `registered_date` date NOT NULL,
  `bio` text COLLATE utf8_bin,
  `address` text COLLATE utf8_bin,
  `gender` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `first_name`, `last_name`, `middle_name`, `email`, `registered_date`, `bio`, `address`, `gender`) VALUES
(10, 'peterporras', '5ebe2294ecd0e0f08eab7690d2a6ee69', 'Peter', 'Porras', 'Alcagno', 'peterporras27@gmail.com', '2019-11-13', 'Nothing much to say anymore.', 'Brgy. Naslo Pototan Iloilo', 'Male'),
(16, 'gherwen', '5ebe2294ecd0e0f08eab7690d2a6ee69', 'Ma. Gherwen', 'Catolico', 'Porras', 'ma.gherwen@gmail.com', '2019-08-27', 'I Love to eat and travel.', 'Golgota St., Janiuay, Iloilo City', 'Female'),
(17, 'pagealcagno', 'e10adc3949ba59abbe56e057f20f883e', 'Peter', 'Porras', 'Alcagno', 'pagealcagno@gmail.com', '2019-11-13', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `blog_id` (`blog_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `uid_2` (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

