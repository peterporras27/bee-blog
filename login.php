<?php 

include($_SERVER['DOCUMENT_ROOT'].'/header.php' );

if (empty($_POST) === false) { // check if form submission isent
    
    $email = $_POST['email'];
    $password = $_POST['password'];

    if ( empty( $email ) || empty( $password ) ) {

        if ( empty($email) ) { $errors[] = 'Kindly enter your email.'; } 
        if ( empty($password) ) { $errors[] = 'Kindly enter your password.'; } 
    
    } else if ($user->email_exists($email) === false) {

        $errors[] = 'The email you entered is not registered.';

    } else {

        if (strlen($password) > 32){ $errors[] = 'Password too long.'; }
        
        $login = $user->login($email, $password);
        
        if ($login === false) {
        
            $errors[] = 'That email and password combination is incorrect.';
        
        } else {
        
            $_SESSION['uid'] = $login;
            header('Location: admin/index.php');
            exit();
        }
    }
}

?>
    <header class="masthead">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>Login</h1>
                        <span class="subheading">Start Blogging with Bee Blogger</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">

            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading" align="center">
                        <h3 class="panel-title">LOGIN</h3>
                    </div>
                    <div class="panel-body">
                        <?php if (empty($errors) === false){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?php echo $app->output_errors($errors); ?>
                            </div>
                        <?php } ?>
                        <form role="form" method="post" action="<?php $app->siteurl(); ?>/login.php">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="text" value="" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-primary btn-block" value="Login">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>

        </div>
    </div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>