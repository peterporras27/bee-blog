<?php 

include($_SERVER['DOCUMENT_ROOT'].'/header.php');

// Redirect if no user id is found.
if (!isset($_GET['uid'])) { header("Location: index.php");exit(); }
$userid = preg_replace('/\D/', '', $_GET['uid']);
// redirect if id is invalid.
if (empty($userid)) { header("Location: index.php"); exit(); }
$loggedin = $blog->get_author($userid);
// redirect if user is not found.
if (!$loggedin) { header("Location: index.php"); exit(); }


?>

<header class="masthead">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1><?php echo ucwords($loggedin['first_name'].' '. $loggedin['last_name']); ?></h1> 
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-10 mx-auto">
			<div class="table-responsive">
				<table class="table table-hover">
					<tbody>

						<?php if ($loggedin['username']): ?>
							<tr>
								<td><b>Username:</b></td>
								<td style="text-align: right;"><?php echo $loggedin['username']; ?></td>
							</tr>
						<?php endif; ?>


						<?php if ($loggedin['first_name']): ?>
							<tr>
								<td><b>First Name:</b></td>
								<td style="text-align: right;"><?php echo $loggedin['first_name']; ?></td>
							</tr>
						<?php endif; ?>


						<?php if ($loggedin['last_name']): ?>
							<tr>
								<td><b>Last Name:</b></td>
								<td style="text-align: right;"><?php echo $loggedin['last_name']; ?></td>
							</tr>
						<?php endif; ?>


						<?php if ($loggedin['middle_name']): ?>
							<tr>
								<td><b>Middle Name:</b></td>
								<td style="text-align: right;"><?php echo $loggedin['middle_name']; ?></td>
							</tr>
						<?php endif; ?>


						<?php if ($loggedin['email']): ?>
							<tr>
								<td><b>Email Address:</b></td>
								<td style="text-align: right;"><?php echo $loggedin['email']; ?></td>
							</tr>
						<?php endif; ?>


						<?php if ($loggedin['gender']): ?>
							<tr>
								<td><b>Gender:</b></td>
								<td style="text-align: right;"><?php echo $loggedin['gender']; ?></td>
							</tr>
						<?php endif; ?>


						<?php if ($loggedin['address']): ?>
							<tr>
								<td><b>Address:</b></td>
								<td style="text-align: right;"><?php echo $loggedin['address']; ?></td>
							</tr>
						<?php endif; ?>

					</tbody>
				</table>

			</div>
			<hr>

			<?php if ($loggedin['bio']): ?>
				<h5>Biography</h5>
				<div>
				<?php echo $loggedin['bio']; ?>
				</div>
			<?php endif; ?>
			<?php if( $userid == $_SESSION['uid'] ): ?>
				<hr>
				<a href="<?php $app->siteurl(); ?>/admin/settings.php">
					<small>
						<i class="fa fa-edit"></i>
						Edit Profile
					</small>
				</a>
			<?php endif; ?>
			<br><br>
			<a class="btn btn-primary btn-block" href="<?php $app->siteurl(); ?>/index.php?uid=<?php echo $loggedin['uid']; ?>">View Blogs</a>

		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>