<?php 

include($_SERVER['DOCUMENT_ROOT'].'/header.php');

if (isset($_GET['id'])) {

	$id = preg_replace('/\D/', '', $_GET['id']);
	$article = $blog->blog_data($id);

	if (!$article) {
		header("Location: index.php");
	  	exit();
	}

	$author = $blog->get_author($article['user_id']);
}

if ($app->is_online() === true) { 

	if (isset($_POST) && empty($_POST) === false) {

		$required_fields = array(
			'comment', 
		);

		$content_err = (empty($_POST['comment']))? ' has-error': '';

		foreach ($_POST as $key => $value) {
			if (empty($value) && in_array($key, $required_fields) === true){
				$errors[] = 'Kindly fill all the required fields.';
				break 1;
			}
		}

		if (empty($errors) === true){

			if (strlen($_POST['comment']) < 6){ 
				$errors[] = 'Your content must be at least 6 characters.';
				$content_err = ' has-error';
			}
		}

		if ( empty($errors) === true ) {

			$register_data = array(
				'comment' => wordwrap($_POST['comment'], 70),
				'user_id' => $_SESSION['uid'],
				'blog_id' => $id,
				'created_at' => date('Y-m-d'),
			);

			$success = $comment->create_comment($register_data); 

			if ($success) {
				header("Location: blog.php?id=$id&success=1");
			} else {
				header("Location: blog.php?id=$id&success=0");
			}
			
		  	exit();
		} 
	}
}
?>

    <header class="masthead" style="background-image: url('<?php $app->siteurl() ?>/assets/img/post-bg.jpg')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>Blogger</h1> 
                    </div>
                </div>
            </div>
        </div>
    </header>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $article['title']; ?></h1>
				<?php echo $article['content']; ?>
				<div class="publisher">
					Posted by <a href="<?php $app->siteurl(); ?>/profile.php?uid=<?php echo $author['uid']; ?>"><?php echo ucwords($author['first_name'] .' ' . $author['last_name']); ?></a> on <i><?php echo date('F j, Y',strtotime($article['created_at'])); ?></i>

					<?php if( $article['user_id'] == $_SESSION['uid'] ): ?>
						|
						<a href="<?php $app->siteurl(); ?>/admin/blogs/edit.php?edit=<?php echo $article['id']; ?>">
							Edit <i class="fa fa-edit"></i>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		
		<?php if ($app->is_online() === true) : ?>

			<div class="row">
				<div class="col-md-12">

					<?php if (isset($_GET['success']) ) : ?>

						<?php if ($_GET['success']==1): ?>

							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								Comment successfully added!
							</div>		
						
						<?php else: ?>

							<div class="alert alert-warning">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								Sorry, there has been an error posting your comment.
							</div>

						<?php endif; ?>

					<?php endif; ?>

					<h3>Comment</h3>
					<form action="" method="POST" role="form">
						<div class="form-group">
							<textarea type="text" rows="5" name="comment" class="form-control" id="" placeholder="Your comment..."></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>

				</div>
			</div>
		<?php else: ?>
		<div class="row">
			<div class="col-md-12">
				<h3>Comments</h3>
				<div class="alert">
					You must <a href="<?php $app->siteurl(); ?>/login.php">login</a> to leave a comment.
				</div>
			</div>
		</div>
		<?php endif; ?>

		<div class="row">
			<div class="col-md-12">
				<hr>
				<?php 
				$page = (!isset($_GET['page'])) ? 1: $_GET['page'];
				$com = $comment->pull_comments( $id, $page );
				?>
				<?php while( $auth = $com['comments']->fetch(PDO::FETCH_ASSOC) ): ?>
					<?php $u = $blog->get_author($auth['user_id']); ?>
					<div class="comment post-preview">
						<div style="margin-bottom: 15px;">
							<strong><a href="<?php $app->siteurl(); ?>/profile.php?uid=<?php echo $u['uid']; ?>"><?php echo $u['first_name'] . ' ' . $u['last_name']; ?></a></strong>
							<small>Posted: <i><?php echo date('F j, Y', strtotime($auth['created_at'])); ?></i></small>
						</div>
						<div><?php echo $auth['comment']; ?></div>
						<hr>
					</div>
					
				<?php endwhile; ?>
				
				<nav aria-label="Page navigation">
					<ul class="pagination">

						<?php for ($page=1; $page <= $com['total_pages'] ; $page++):  ?>
							
							<?php if ($com['total_pages'] == $page ): ?>
								<li>
									<a href="<?php echo "?id=$id&page=$page"; ?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							<?php elseif ($page==1) : ?>
								<li>
									<a href="<?php echo "?id=$id&page=$page"; ?>" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
							<?php else : ?>
								<li>
									<a href='<?php echo "?id=$id&page=$page"; ?>' class="links">
										<?php  echo $page; ?>
								 	</a>
								</li>
							<?php endif; ?>
							
						<?php endfor; ?>
						
					</ul>
				</nav>
				
			</div>
		</div>
	</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>