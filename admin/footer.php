</div>

    <script src="<?php $app->siteurl(); ?>/assets/js/jquery.min.js"></script>
    <script src="<?php $app->siteurl(); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php $app->siteurl(); ?>/assets/js/metisMenu.min.js"></script>
    <script src="<?php $app->siteurl(); ?>/assets/js/summernote.min.js"></script>
    <script src="<?php $app->siteurl(); ?>/assets/js/startmin.js"></script>
    <script>
        jQuery(document).ready(function(){

            if (jQuery('.summernote').length > 0) {

                jQuery('.summernote').summernote({focus: true});

                jQuery('#save').click(function(event) {
                    jQuery('[name="content"]').html(jQuery('.summernote').code());
                });
            }

            jQuery('#delete-blog').on('shown.bs.modal', function (event) {
                var button = jQuery(event.relatedTarget) // Button that triggered the modal
                var blogid = button.data('blogid');
                var url = '<?php $app->siteurl(); ?>/admin/blogs/delete.php?id='+blogid;
                jQuery(this).find('#delete').attr('href',url);
            });
            
       	});
    </script>

</body>
</html>