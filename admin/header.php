<?php include( $_SERVER['DOCUMENT_ROOT'] . '/app/init.php'); 

// check if user is logged in
if ($app->is_online() === true) { 
    
    $session_uid = $_SESSION['uid']; // get user session ID
    // retrieve user information
    $loggedin = $user->user_data(
        $session_uid, 
        'uid', 
        'username', 
        'first_name', 
        'last_name', 
        'email',
        'middle_name',
        'bio',
        'address',
        'gender'
    );
    
} else {
    
    // redirect user to login page if the user is not logged in
    header('Location: ../login.php');
    exit();
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Blogger</title>
    <link href="<?php $app->siteurl(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/metisMenu.min.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/timeline.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/startmin.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/morris.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php $app->siteurl(); ?>/assets/css/summernote.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/summernote-bs3.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/style.css" rel="stylesheet">
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Blogger</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <!-- Top Navigation: Left Menu -->
        <ul class="nav navbar-nav navbar-left navbar-top-links">
            <li><a href="<?php $app->siteurl(); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
        </ul>

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <?php echo $loggedin['username']; ?> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="<?php $app->siteurl(); ?>/profile.php?uid=<?php echo $_SESSION['uid']; ?>"><i class="fa fa-user fa-fw"></i> Profile</a>
                    </li>
                    <li><a href="<?php $app->siteurl(); ?>/admin/settings.php"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php $app->siteurl(); ?>/admin/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    <li<?php $app->activeNav('/admin/index.php'); ?>>
                        <a href="<?php $app->siteurl(); ?>/admin/index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li<?php $app->activeNav('/admin/blogs/index.php'); ?>>
                        <a href="<?php $app->siteurl(); ?>/admin/blogs/index.php"><i class="fa fa-edit fa-fw"></i> Blogs</a>
                    </li>
                    
                </ul>

            </div>
        </div>
    </nav>

