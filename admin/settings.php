<?php 

include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php'); 

$author = $blog->get_author($_SESSION['uid']);

$username_err = '';
$password_err = '';
$password_repeat_err = '';
$first_name_err = '';
$last_name_err = '';
$email_err = '';
$success = (isset($_GET['success'])) ? $_GET['success']:0;

if (isset($_POST) && empty($_POST) === false) {

	$required_fields = array(
		'username', 
		'first_name', 
		'last_name', 
		'email'
	);

	$username_err	= (empty($_POST['username']))? ' has-error': '';

	if ( !empty($_POST['password']) ) {
		$password_repeat_err  = (empty($_POST['password_repeat']))? ' has-error': '';	
	}
	
	$first_name_err = (empty($_POST['first_name']))? ' has-error': '';
	$last_name_err 	= (empty($_POST['last_name']))? ' has-error': '';
	$email_err		= (empty($_POST['email']))? ' has-error': '';

	foreach ($_POST as $key => $value) {
		if (empty($value) && in_array($key, $required_fields) === true){
			$errors[] = 'Kindly fill all the required fields.';
			break 1;
		}
	}

	if (empty($errors) === true){

		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		  	$errors[] = 'Invalid email address format.';
			$email_err = ' has-error';
		}
		
		if ( $_POST['email'] != $author['email'] ) {
			if (email_exists($_POST['email']) != false){
				$errors[] = 'Your email is already in use.';
				$email_err = ' has-error';
			}	
		}
		
		if ( $_POST['username'] != $author['username'] ) {
			if ($user->user_exists($_POST['username']) != false){
				$errors[] = 'Sorry, but the username \''.strip_tags($_POST['username']).'\' is already taken.';
				$username_err = ' has-error';
			}
		}

		if (preg_match("/\\s/", $_POST['username']) == true){
			$errors[] = 'Your username must not contain any spaces.';
			$username_err = ' has-error';
		}

		if (strlen($_POST['username']) > 32){
			$errors[] = 'Your username must be at lower than 32 characters.';
			$username_err = ' has-error';
		}

		if ( !empty($_POST['password']) ) {
			if (strlen($_POST['password']) < 6){
				$errors[] = 'Your password must be at least 6 characters.';
				$password_err = ' has-error';
				$password_repeat_err = ' has-error';
			}

			if ($_POST['password'] !== $_POST['password_repeat']){
				$errors[] = 'Your passwords do not match.';
				$password_err = ' has-error';
				$password_repeat_err = ' has-error';
			}
		}
	}

	if ( empty($errors) === true ) {

		$register_data = array(
			'username'        => strtolower($_POST['username']),
			'first_name'      => ucwords($_POST['first_name']),
			'last_name'       => ucfirst($_POST['last_name']),
			'middle_name'     => ucfirst($_POST['middle_name']),
			'registered_date' => date('Y-m-d'),
			'uid'			  => $author['uid']
		);

		if ( !empty($_POST['password']) ) {
			$register_data['password'] = $_POST['password'];
		}

		if (isset($_POST['gender'])) {
			$register_data['gender'] = $_POST['gender'];
		}

		if (isset($_POST['bio'])) {
			$register_data['bio'] = addslashes($_POST['bio']);
		}

		if (isset($_POST['address'])) {
			$register_data['address'] = addslashes($_POST['address']);
		}

		if ( $_POST['email'] != $author['email'] ) {
			$register_data['email'] = $_POST['email'];
		}

		if ( $_POST['username'] != $author['username'] ) {
			$register_data['username'] = $_POST['username'];
		}

		$status = $user->update_user($register_data); 
		if ($status) {
			header("Location: settings.php?success=1");
		} else {
			header("Location: settings.php?success=0");
		}
		
	} 
} 
?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Profile Settings</h1>
            </div>
        </div>

		<form action="" method="POST" role="form">

			<div class="row">
				<div class="col-md-12">
					
					<?php if (empty($errors) === false){ ?>
						
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $app->output_errors($errors); ?>
						</div>
					<?php } ?>

					<?php if ($success==1) { ?>
						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							Account successfully updated!
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					
				
					<div class="form-group">
						<label for="">Username<sup style="color:red;">*</sup></label>
						<input type="text" class="form-control" name="username" value="<?php echo $author['username']; ?>" placeholder="Username">
					</div>

					<div class="form-group">
						<label for="">First Name<sup style="color:red;">*</sup></label>
						<input type="text" class="form-control" name="first_name" value="<?php echo $author['first_name']; ?>" placeholder="First Name">
					</div>

					<div class="form-group">
						<label for="">Last Name<sup style="color:red;">*</sup></label>
						<input type="text" class="form-control" name="last_name" value="<?php echo $author['last_name']; ?>" placeholder="Last Name">
					</div>

					<div class="form-group">
						<label for="">Middle Name</label>
						<input type="text" class="form-control" name="middle_name" value="<?php echo $author['middle_name']; ?>" placeholder="Middle Name">
					</div>
					
					<hr>

					<div class="form-group">
						<label for="">Password</label>
						<input type="password" class="form-control" name="password" value="" placeholder="Password">
					</div>

					<div class="form-group">
						<label for="">Confirm Password</label>
						<input type="password" class="form-control" name="password_repeat" value="" placeholder="Confirm Password">
					</div>

				</div>
				<div class="col-md-8 col-sm-8 col-xs-12">
					
					
					<div class="form-group">
						<label for="">Address</label>
						<input type="text" class="form-control" name="address" value="<?php echo $author['address']; ?>" placeholder="Address">
					</div>
				
					<div class="form-group">
						<label for="">Gender</label>
						<select type="text" class="form-control" name="gender">
							<option value="Male"<?php echo ($author['gender']=='Male') ? ' selected':''; ?>>Male</option>
							<option value="Female"<?php echo ($author['gender']=='Female') ? ' selected':''; ?>>Female</option>
						</select>
					</div>

					<div class="form-group">
						<label for="">Email<sup style="color:red;">*</sup></label>
						<input type="email" class="form-control" name="email" value="<?php echo $author['email']; ?>" placeholder="Email">
					</div>


					<div class="form-group">
						<label for="">Biography</label>
						<textarea rows="5" class="form-control" name="bio" placeholder=""><?php echo $author['bio']; ?></textarea>
					</div>

					<button type="submit" class="btn btn-primary">Save <i class="glyphicon glyphicon-floppy-disk"></i></button>

				</div>
			</div>

		</form>

    </div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); ?>