<?php include( $_SERVER['DOCUMENT_ROOT'] . '/admin/header.php'); 

if (isset($_GET['edit'])) {

	$id = preg_replace('/\D/', '', $_GET['edit']);
	$article = $blog->blog_data($id);

	if (!$article) {
		header("Location: index.php");
	  	exit();
	}

	// check if user owns this blog.
	if ($article['user_id'] != $_SESSION['uid']) {
		header("Location: index.php");
		exit();
	}
}

$title_err = '';
$content_err = '';
$title = isset($_POST['title']) ? strip_tags($_POST['title']): '';
$content = isset($_POST['content']) ? htmlspecialchars($_POST['content']):'';
$errors = array();
$success = isset($_GET['success']) ? 1:'';

if (isset($_POST) && empty($_POST) === false) {

	$required_fields = array(
		'title', 
		'content', 
	);

	$title_err = (empty($_POST['title']))? ' has-error': '';
	$content_err = (empty($_POST['content']))? ' has-error': '';

	foreach ($_POST as $key => $value) {
		if (empty($value) && in_array($key, $required_fields) === true){
			$errors[] = 'Kindly fill all the required fields.';
			break 1;
		}
	}

	if (empty($errors) === true){

		if (strlen($_POST['title']) < 3) {
			$errors[] = 'Your title must be greater than 3 characters.';
			$title_err = ' has-error';
		}

		if (strlen($_POST['content']) < 6){ 
			$errors[] = 'Your content must be at least 6 characters.';
			$content_err = ' has-error';
		}
	}

	if ( empty($errors) === true ) {

		$update_data = array(
			'id' 		 => $id,
			'title' 	 => addslashes($_POST['title']),
			'content' 	 => addslashes(wordwrap($_POST['content'],70)),
			'user_id' 	 => $_SESSION['uid'],
			'created_at' => date('Y-m-d')
		);

		$status = $blog->update_blog($update_data); 
		if ($status) { 
			header("Location: edit.php?edit=$id&success=1");
	  		exit();
		}
	} 
}
?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Blog</h1>
            </div>
        </div>

        
		<div class="row">
			<div class="col-md-12">

				<?php if (empty($errors) === false){ ?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $app->output_errors($errors); ?>
					</div>
				<?php } ?>

				<?php if (!empty($success)) { ?>
					<div class="alert alert-info alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Post updated successfully!
					</div>
				<?php } ?>

				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Details</h3>
					</div>
					<div class="panel-body">
						
						<form action="" method="POST" role="form">
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							<div class="form-group">
								<label for="">Title</label>
								<input type="text" name="title" class="form-control" value="<?php echo $article['title']; ?>" id="" placeholder="">
							</div>
						
							<div class="form-group">
								<label for="">Content</label>
								<div class="summernote"><?php echo $article['content']; ?></div>
								<textarea type="text" style="display: none;" name="content" class="form-control" id="" placeholder="" rows="5"><?php echo $article['content']; ?></textarea>
							</div>
						
							<button id="save" type="submit" class="btn btn-primary">
								Save <i class="glyphicon glyphicon-floppy-disk"></i>
							</button>

							<a href="<?php $app->siteurl(); ?>/blog.php?id=<?php echo $article['id']; ?>" class="btn btn-info">
								View <i class="fa fa-eye"></i>
							</a>
						</form>

					</div>
				</div>
			</div>
		</div>

    </div>
</div>




<?php include($_SERVER['DOCUMENT_ROOT'] . '/admin/footer.php'); ?>