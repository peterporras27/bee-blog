<?php include( $_SERVER['DOCUMENT_ROOT'] . '/admin/header.php'); ?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                	Blogs
                	<a href="<?php $app->siteurl(); ?>/admin/blogs/create.php" class="btn btn-success pull-right">Create Blog <i class="glyphicon glyphicon-plus"></i></a>
                </h1>
                
				<div class="clear"></div>
            </div>
        </div>
        
		<div class="row">
			
			<div class="col-md-12">
						
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">List</h3>
					</div>
					<div class="panel-body" style="background: #fff;padding:0;">
						<?php if (isset($_GET['delete'])) : ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Success!</strong> Blog successfully deleted.
							</div>
						<?php endif; ?>
						<?php 

						$userid = ( isset($_SESSION['uid']) ) ? preg_replace('/\D/', '', $_SESSION['uid']): null;
						$page = (!isset($_GET['page'])) ? 1: $_GET['page'];
						$data = $blog->pull_blogs( $userid, $page );

						if ($data['blogs']->rowCount() >= 1): ?>

							<div class="table-responsive table-bordered">
				
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Title</th>
											<th>Date Published</th>
										</tr>
									</thead>
									<tbody>
										
										<?php while($res = $data['blogs']->fetch(PDO::FETCH_ASSOC)): ?>
											<tr>
												<td>
													<a href="<?php $app->siteurl(); ?>/admin/blogs/edit.php?edit=<?php echo $res['id']; ?>"><h4><?php echo $res['title']; ?></h4></a>
													<div>
														<a href="<?php $app->siteurl(); ?>/admin/blogs/edit.php?edit=<?php echo $res['id']; ?>" class="btn btn-link btn-xs">Edit</a> 
														| 
														<a target="_blank" href="<?php $app->siteurl(); ?>/blog.php?id=<?php echo $res['id']; ?>" class="btn btn-link btn-xs">View</a>
														|
														<a data-toggle="modal" href='#delete-blog' data-blogid="<?php echo $res['id']; ?>" href="#" class="btn btn-link btn-xs">Delete</a>
													</div>
												</td>
												<td><?php echo date('F j, Y',strtotime($res['created_at'])); ?></td>
											</tr>	
										<?php endwhile; ?>
										
									</tbody>
								</table>
							</div>

							<nav aria-label="Page navigation" style="margin-left: 15px;">
								<ul class="pagination">

									<?php for ($page=1; $page <= $data['total_pages'] ; $page++):  ?>
										
										<?php if ($data['total_pages'] == $page ): ?>
											<li>
												<a href="<?php echo "?page=$page"; ?>" aria-label="Next">
													<span aria-hidden="true"><i class="glyphicon glyphicon-menu-right"></i></span>
												</a>
											</li>
										<?php elseif ($page==1) : ?>
											<li>
												<a href="<?php echo "?page=$page"; ?>" aria-label="Previous">
													<span aria-hidden="true"><i class="glyphicon glyphicon-menu-left"></i></span>
												</a>
											</li>
										<?php else : ?>
											<li>
												<a href='<?php echo "?page=$page"; ?>' class="links">
													<?php  echo $page; ?>
											 	</a>
											</li>
										<?php endif; ?>
										
									<?php endfor; ?>
									
								</ul>
							</nav>

						<?php else: ?>
								
						<div class="alert alert-warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>No post to show!</strong> There are no blog post to show at the moment. you can start creating now.
						</div>

						<?php endif; ?>


					</div>
				</div>
					

			</div>
		</div>


    </div>
</div>


<div class="modal fade" id="delete-blog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Blog?</h4>
			</div>
			<div class="modal-body">
				<div class="panel panel-warning" style="margin-bottom: 0;">
					<div class="panel-heading">
						<h3 class="panel-title">Warning</h3>
					</div>
					<div class="panel-body">
						Are you sure you wish to delete this blog?
					</div>
				</div>
			</div>
			<div class="modal-footer" style="margin-top: 0;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<a id="delete" type="button" href="" class="btn btn-primary">Delete</a>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/admin/footer.php'); ?>