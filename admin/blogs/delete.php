<?php 

include($_SERVER['DOCUMENT_ROOT'] . '/app/init.php');

if (isset($_GET['id'])) {

	$id = preg_replace('/\D/', '', $_GET['id']);
	$article = $blog->blog_data($id);

	if ($article) {

		// check if user owns this blog.
		if ($article['user_id'] != $_SESSION['uid']) {
			header("Location: index.php");
			exit();
		}
		
		$success = $blog->delete_blog($id);

		if ($success) {
			header("Location: index.php?delete=1");
		} else {
			header("Location: index.php");
		}

	} else {
		header("Location: index.php");
	}

	exit();
}