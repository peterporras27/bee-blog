<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/header.php'); ?>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
            </div>

            <div class="jumbotron">
				<div class="container">
					<h1>Welcome!</h1>
					<p>You may now start creating your own blog.</p>
					<p>
						<a href="<?php $app->siteurl(); ?>/admin/blogs/" class="btn btn-primary btn-lg">Start now!</a>
					</p>
				</div>
			</div>

        </div>
    </div>


<?php include($_SERVER['DOCUMENT_ROOT'].'/admin/footer.php'); ?>