<?php 

include($_SERVER['DOCUMENT_ROOT'].'/header.php');  

$username_err = '';
$password_err = '';
$password_repeat_err = '';
$first_name_err = '';
$last_name_err = '';
$email_err = '';
$success = '';

if (isset($_POST) && empty($_POST) === false) {

	$required_fields = array(
		'username', 
		'password', 
		'password_repeat', 
		'first_name', 
		'last_name', 
		'email'
	);

	$username_err         = (empty($_POST['username']))? ' has-error': '';
	$password_err         = (empty($_POST['password']))? ' has-error': '';
	$password_repeat_err  = (empty($_POST['password_repeat']))? ' has-error': '';
	$first_name_err       = (empty($_POST['first_name']))? ' has-error': '';
	$last_name_err        = (empty($_POST['last_name']))? ' has-error': '';
	$email_err      	  = (empty($_POST['email']))? ' has-error': '';

	foreach ($_POST as $key => $value) {
		if (empty($value) && in_array($key, $required_fields) === true){
			$errors[] = 'Kindly fill all the required fields.';
			break 1;
		}
	}

	if (empty($errors) === true){

		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		  	$errors[] = 'Invalid email address format.';
			$email_err = ' has-error';
		}
		
		if ($user->email_exists($_POST['email']) != false){
			$errors[] = 'Your email is already in use.';
			$email_err = ' has-error';
		}

		if ($user->user_exists($_POST['username']) != false){
			$errors[] = 'Sorry, but the username \''.strip_tags($_POST['username']).'\' is already taken.';
			$username_err = ' has-error';
		}

		if (preg_match("/\\s/", $_POST['username']) == true){
			$errors[] = 'Your username must not contain any spaces.';
			$username_err = ' has-error';
		}

		if (strlen($_POST['username']) > 32){
			$errors[] = 'Your username must be at lower than 32 characters.';
			$username_err = ' has-error';
		}

		if (strlen($_POST['password']) < 6){
			$errors[] = 'Your password must be at least 6 characters.';
			$password_err = ' has-error';
			$password_repeat_err = ' has-error';
		}

		if ($_POST['password'] !== $_POST['password_repeat']){
			$errors[] = 'Your passwords do not match.';
			$password_err = ' has-error';
			$password_repeat_err = ' has-error';
		}
	}

	if ( empty($errors) === true ) {

		$register_data = array(
			'username'        => strtolower($_POST['username']),
			'password'        => $_POST['password'],
			'first_name'      => ucwords($_POST['first_name']),
			'last_name'       => ucfirst($_POST['last_name']),
			'middle_name'     => ucfirst($_POST['middle_name']),
			'email'           => $_POST['email'],
			'registered_date' => date('Y-m-d'),
		);

		$status = $user->register_user($register_data); 
		if ($status) {
			$success = 1;
		} else {
			$success = 0;
		}
		
	} 
} 

?>
	<header class="masthead">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>Signup</h1>
                        <span class="subheading">Start Blogging with Bee Blogger</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

<div class="container">
	<div class="row">

		<div class="col-lg-8 col-md-10 mx-auto">
			<div class="panel signup-panel panel-default">
				<div class="panel-body">

					<?php if (empty($errors) === false){ ?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $app->output_errors($errors); ?>
						</div>
					<?php } ?>

					<?php if (!empty($success)) { ?>
						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							Account has been registered successfully! you may now <a href="<?php $app->siteurl(); ?>/login.php">Login</a>
						</div>
					<?php } ?>

					<form role="form" id="addteacher" method="post">
						<div class="col-md-12">
							<div class="form-group<?php echo $first_name_err; ?>">
								<label>First Name*</label> <span><i><small>(required)</small></i></span>
								<input type="text" name="first_name" class="form-control">
							</div>
							<div class="form-group<?php echo $last_name_err; ?>">
								<label>Last Name*</label> <span><i><small>(required)</small></i></span>
								<input type="text" name="last_name" class="form-control">
							</div>
							<div class="form-group">
								<label>Middle Name</label> <span><i><small>(optional)</small></i></span>
								<input type="text" name="middle_name" class="form-control">
							</div>
							<div class="form-group<?php echo $email_err; ?>">
								<label>Email*</label> <span><i><small>(required)</small></i></span>
								<input type="email" name="email" class="form-control">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group<?php echo $username_err; ?>">
								<label>Username*</label> <span><i><small>(required)</small></i></span>
								<input type="text" class="form-control" name="username">
							</div>
							<div class="form-group<?php echo $password_err; ?>">
								<label>Password*</label> <span><i><small>(Must be at least 6 character or higher)</small></i></span>
								<input type="password" name="password" class="form-control">
							</div>
							<div class="form-group<?php echo $password_repeat_err; ?>">
								<label>Repeat Password*</label> <span><i><small>(required)</small></i></span>
								<input type="password" name="password_repeat" class="form-control">
							</div>
							<input type="submit" class="btn btn-lg btn-info btn-block" value="Register">
						</div>
						
					</form>


				</div>
			</div><!-- panel -->
		</div><!-- col -->

	</div><!-- row -->
</div><!-- container -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>