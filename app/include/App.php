<?php

class App {

	protected $db;

	public function __construct()
	{
		$this->db = new DB();
		$this->db = $this->db->get_obj();
	}
	
	/*
	* checks if user is logged in. returns TRUE or FALSE
	*/
	public function is_online() {
		return (isset($_SESSION['uid'])) ? true : false;
	}

	/*
	* Trims text.
	*/
	public function shorten_text($text, $max_length = 140, $cut_off = '...', $keep_word = false)
	{
	    if(strlen($text) <= $max_length) {
	        return $text;
	    }

	    if(strlen($text) > $max_length) {
	        if($keep_word) {
	            $text = substr($text, 0, $max_length + 1);

	            if($last_space = strrpos($text, ' ')) {
	                $text = substr($text, 0, $last_space);
	                $text = rtrim($text);
	                $text .=  $cut_off;
	            }
	        } else {
	            $text = substr($text, 0, $max_length);
	            $text = rtrim($text);
	            $text .=  $cut_off;
	        }
	    }

	    return $text;
	}

	/*
	* echo's website url
	*/
	public function siteurl()
	{
		echo "http://$_SERVER[HTTP_HOST]";
	}

	/*
	* returns website url
	*/
	public function site_url()
	{
		return "http://$_SERVER[HTTP_HOST]";
	}

	/*
	* echo's Current page url
	*/
	public function geturl()
	{
		echo "http://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]";
	}

	/*
	* returns current page url
	*/
	public function get_url()
	{
		return "http://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]";
	}

	/*
	* adding a class in the navigation menu if selected page is active.
	*/
	public function activeNav($pageurl)
	{
		echo ($pageurl==$_SERVER['PHP_SELF'])? ' class="active nav-item"':' class="nav-item"';
	}

	/*
	* displays all error in html form
	*/
	public function output_errors($errors)
	{
		return '<ul style="list-style:none;padding:0;"><li>'.implode('</li><li>', $errors).'</li></ul>';
	}

	/*** starting the session ***/
	public function get_session(){
		return $_SESSION['login'];
	}
}