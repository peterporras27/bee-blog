<?php
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'blog');

class DB {

	public $connection;

	function __construct(){

		try {

		    $this->connection = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_DATABASE, DB_USERNAME, DB_PASSWORD);
			// set the PDO error mode to exception
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    
		} catch(PDOException $e) {

			die('Database Connection failed: ' . $e->getMessage());
		}
	}
	
	function get_obj(){
		return $this->connection;
	}

}
