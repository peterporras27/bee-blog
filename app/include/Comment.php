<?php 

class Comment {

	protected $db;

	public function __construct()
	{
		$this->db = new DB();
		$this->db = $this->db->get_obj();
	}

	public function pull_comments( $blogid, $page )
	{
		$limit = 5;

		$myblogs = ( $blogid ) ? "WHERE `blog_id` = $blogid":"";
		
		$s = $this->db->prepare("SELECT * FROM `comments` $myblogs ORDER BY `id` DESC");
		
		$s->execute();

		$total_results = $s->rowCount();

		$total_pages = ceil($total_results/$limit);
	    
	    $page = preg_replace('/\D/', '', $page );
	    
	    $page = (empty($page)) ? 1: $page;
		
		$starting_limit = ($page-1)*$limit;
		
		$r = $this->db->prepare("SELECT * FROM `comments` $myblogs ORDER BY `id` DESC LIMIT $starting_limit, $limit");
		
		$r->execute();

		return array(
			'comments' => $r,
			'total_pages' => $total_pages,
			'total_results' => $total_results
		);
	}

	/*
	* Create Comment
	*/
	public function create_comment($register_data) 
	{
		array_walk($register_data, array($this, 'array_sanitize'));
		
		$fields = '`' . implode('`, `', array_keys($register_data)) . '`';
		
		$data = '\'' . implode('\', \'', $register_data) . '\'';
		
		$blog = $this->db->prepare("INSERT INTO `comments` ($fields) VALUES ($data)");
		
		return $blog->execute();
	}

	// clean arrays to convert HTML tags
	public function array_sanitize(&$item){
		
		$return = array();

		if (is_array($item)) {
			foreach ($item as $key => $val) {
				$return[$key] = addslashes( $val );
			}
		}
	    return $return;
	}
}