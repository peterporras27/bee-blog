<?php 

class User{

	protected $db;

	public function __construct()
	{
		$this->db = new DB();
		$this->db = $this->db->get_obj();
	}

	/*
	* Register a user
	*/
	public function register_user($register_data) 
	{
		array_walk($register_data, array($this, 'array_sanitize'));
		$register_data['password'] = md5($register_data['password']);
		$fields = '`' . implode('`, `', array_keys($register_data)) . '`';
		$data = '\'' . implode('\', \'', $register_data) . '\'';
		$user = $this->db->prepare("INSERT INTO `users` ($fields) VALUES ($data)");
		return $user->execute();
	}

	/*
	* checks if the user is registered
	*/
	public function user_exists($username) 
	{
		$username = $this->sanitize($username);
		$user = $this->db->prepare("SELECT * FROM `users` WHERE `username` = '$username'");
		$user->execute();
		return $user->fetch(PDO::FETCH_ASSOC);
	}

	/*
	* checks if the user is email is registered
	*/
	public function email_exists($email) 
	{	
		$email = $this->sanitize($email);
		$mail = $this->db->prepare("SELECT * FROM `users` WHERE `email` = '$email'");
		$mail->execute();
		return $mail->fetch(PDO::FETCH_ASSOC);
	}

	/*
	* retrieves user ID using username
	*/
	public function user_id_from_email($email) 
	{	
		$email = $this->sanitize($email);
		$user = $this->db->query("SELECT `uid` FROM `users` WHERE `email` = '$email'");
		return $user->fetch(PDO::FETCH_ASSOC);
	}


	/*
	* Login checking function. returns either true or false
	*/
	public function login($email, $password) 
	{	
		$uid = $this->user_id_from_email($email);
		$user = $this->user_data($uid['uid'], 'password');
		$password = md5($password);

		if ( is_null( $user ) ) {
			return false;
		}

		return ( $password == $user['password'] )? $uid['uid'] : false;
	}

	/*
	* retrieves user data
	* @params $uid
	*/
	public function user_data($uid) 
	{	
		$data = array();
		$uid = (int) $uid;

		$func_num_args = func_num_args();
		$func_get_args = func_get_args();

		if ($func_num_args > 1){

			unset($func_get_args[0]);
			$fields = '`'.implode('`, `', $func_get_args).'`';

			$data = $this->db->query("SELECT $fields FROM `users` WHERE `uid` = $uid");
			return $data->fetch(PDO::FETCH_ASSOC);
		}
	}

	public function update_user($tinfo)
	{
		array_walk($tinfo, array($this, 'array_sanitize'));

		$uid = $tinfo['uid'];

		$str ="";
		$limit = count($tinfo);
		$counter = 1;
		foreach ($tinfo as $key => $val) {
			$comma = ( $limit == $counter ) ? '':', ';
			if ($key == 'password') { $val = md5($val); }
			$str .= "`$key` = '$val'$comma";
			$counter++;
		}

		$user = $this->db->prepare("UPDATE `users` SET $str WHERE `users`.`uid` = $uid");
		return $user->execute();
	}

	// clean arrays to convert HTML tags
	public function array_sanitize(&$item){
		$return = array();

		if (is_array($item)) {
			foreach ($item as $key => $val) {
				$return[$key] = addslashes( $val );
			}
		}
	    return $return;
	}

	public function sanitize($data){
		return addslashes( $data );
	}


	public function user_logout() {

	    $_SESSION['uid'] = false;
		unset($_SESSION);
	    session_destroy();
    }
}