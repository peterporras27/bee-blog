<?php 

class Blog {

	protected $db;

	public function __construct()
	{
		$this->db = new DB();
		$this->db = $this->db->get_obj();
	}

	public function pull_blogs( $userid, $page )
	{
		$limit = 5;

		$myblogs = ( $userid ) ? "WHERE `user_id` = $userid":"";
		$s = $this->db->prepare("SELECT * FROM `blogs` $myblogs ORDER BY `id` DESC");
		$s->execute();

		$total_results = $s->rowCount();
		$total_pages = ceil($total_results/$limit);
		$starting_limit = ($page-1)*$limit;
		$r = $this->db->prepare("SELECT * FROM `blogs` $myblogs ORDER BY `id` DESC LIMIT $starting_limit, $limit");
		$r->execute(); 

		return array(
			'blogs' => $r,
			'total_pages' => $total_pages,
			'total_results' => $total_results
		);
	}



	/*
	* Create Blog
	*/
	public function create_blog($register_data) 
	{
		array_walk($register_data, array($this, 'array_sanitize'));
		$fields = '`' . implode('`, `', array_keys($register_data)) . '`';
		$data = '\'' . implode('\', \'', $register_data) . '\'';
		$blog = $this->db->prepare("INSERT INTO `blogs` ($fields) VALUES ($data)");
		$blog->execute(); 
		return $this->db->lastInsertId();
	}

	/*
	Retrive blog data
	*/
	public function blog_data($uid) 
	{
		$blog = $this->db->prepare("SELECT * FROM `blogs` WHERE `id` = $uid");
		$isok = $blog->execute();
		if ($isok) {
			return $blog->fetch(PDO::FETCH_ASSOC);	
		} else {
			return null;
		}	
	}

	/*
	Retrive blog data
	*/
	public function get_blogs( $id = null ) 
	{
		if (is_null($id) ) {
			$blog = $this->db->prepare("SELECT * FROM `blogs` ORDER BY `id` DESC");	
		} else {
			$blog = $this->db->prepare("SELECT * FROM `blogs` WHERE `user_id` = $id ORDER BY `id` DESC");
		}

		$isok = $blog->execute();

		if ( $isok ) {
			return $blog->fetchAll(PDO::FETCH_ASSOC);	
		} else {
			return null;
		}	
	}

	/*
	* Pull user author
	*/
	public function get_author($uid) 
	{
		$user = $this->db->prepare("SELECT * FROM `users` WHERE `uid` = $uid");
		$user->execute();
		return $user->fetch();
	}

	public function update_blog($blog)
	{
		array_walk($blog, array($this, 'array_sanitize'));
		$id 	 = $blog['id'];
		$title 	 = $blog['title'];
		$content = $blog['content'];

		$blog = $this->db->prepare("UPDATE `blogs` SET `title` = '$title', `content` = '$content' WHERE `id` = $id");
		return $blog->execute();
	}

	/*
	* Delete blog
	*/
	public function delete_blog($id)
	{
		$blog = $this->db->prepare("DELETE FROM `blogs` WHERE  `id` = $id");
		return $blog->execute();
	}

	// clean arrays to convert HTML tags
	public function array_sanitize(&$item){
		$return = array();

		if (is_array($item)) {
			foreach ($item as $key => $val) {
				$return[$key] = addslashes( $val );
			}
		}
	    return $return;
	}
}