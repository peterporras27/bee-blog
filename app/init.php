<?php 

session_start();

include( $_SERVER['DOCUMENT_ROOT'] . "/app/include/DB.php" );
include( $_SERVER['DOCUMENT_ROOT'] . "/app/include/App.php" );
include( $_SERVER['DOCUMENT_ROOT'] . "/app/include/User.php" );
include( $_SERVER['DOCUMENT_ROOT'] . "/app/include/Blog.php" );
include( $_SERVER['DOCUMENT_ROOT'] . "/app/include/Comment.php" );

$db = new DB();
$app = new App();
$user = new User();
$blog = new Blog();
$comment = new Comment();