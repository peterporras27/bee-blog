<?php include($_SERVER['DOCUMENT_ROOT'].'/app/init.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Blogger - Be a blogger</title>
    <link href="<?php $app->siteurl(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href="<?php $app->siteurl(); ?>/assets/css/clean-blog.min.css" rel="stylesheet">
    <link href="<?php $app->siteurl(); ?>/assets/css/style.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="<?php $app->siteurl(); ?>"></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <?php if ($app->is_online() === true) :  ?>
                        <li<?php $app->activeNav('/index.php'); ?>><a href="<?php $app->siteurl(); ?>">Home</a></li>
                        <li<?php $app->activeNav('/admin/index.php'); ?>><a href="<?php $app->siteurl(); ?>/admin/index.php">Dashboard</a></li>
                        <li<?php $app->activeNav('/index.php'); ?>><a href="<?php $app->siteurl(); ?>/index.php?uid=<?php echo $_SESSION['uid']; ?>">My Blogs</a></li>
                        <li<?php $app->activeNav('/admin/lgout.php'); ?>><a href="<?php $app->siteurl(); ?>/admin/logout.php">Logout</a></li>
                    <?php else: ?>
                        <li<?php $app->activeNav('/index.php'); ?>><a href="<?php $app->siteurl(); ?>">Home</a></li>
                        <li<?php $app->activeNav('/login.php'); ?>><a href="<?php $app->siteurl(); ?>/login.php">Login</a></li>
                        <li<?php $app->activeNav('/signup.php'); ?>><a href="<?php $app->siteurl(); ?>/signup.php">Signup</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
    